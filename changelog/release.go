package changelog

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strconv"
)

type Release struct {
	ReleaseVersion string
	Configuration  Configuration
	PathToScan     string
	PathToWrite    string
}

func (p Release) UseAllEntries() []Entry {
	var Entries []Entry

	files, err := ioutil.ReadDir(p.PathToScan)
	if err != nil {
		log.Fatal("Changelog directory not found : " + err.Error())
	}

	for _, f := range files {
		if filepath.Ext(p.PathToScan+"/"+f.Name()) == ".yml" {
			Entries = append(Entries, LoadEntry(p.PathToScan+"/"+f.Name()))
		}
	}

	return Entries
}

func (p Release) GetTypes(Entries []Entry) map[string][]Entry {
	Types := make(map[string]([]Entry))

	for i := 0; i < len(Entries); i++ {
		Types[Entries[i].Type] = append(Types[Entries[i].Type], Entries[i])
	}

	return Types
}

func (p Release) WriteReleaseVersion() string {
	return p.Configuration.ReleaseHeading + " " + p.ReleaseVersion
}

func (p Release) WriteTypeEntries(Type string, Entries []Entry) string {
	Text := p.Configuration.TypeHeading + " " + Type + " (" + strconv.Itoa(len(Entries)) + " change"
	if len(Entries) > 1 {
		Text += "s"
	}
	Text += ")"

	return Text
}

func (p Release) Create() bool {
	Entries := p.UseAllEntries()
	Types := p.GetTypes(Entries)
	fmt.Println(p.WriteReleaseVersion())
	for Type, Entries := range Types {
		fmt.Println(p.WriteTypeEntries(Type, Entries))
	}
	fmt.Println(Types)
	return true
}

// function aggregate(newVersion, fileNames) {
// 		if (!argv.dryRun) {
// 			console.log(`converting ${fileNames.length} files`)
// 		}
// 		const types = {}
// 		const files = fileNames.map(fileName => path.join(pathToScan, fileName))
// 		files.forEach(filePath => {
// 			const content = fs.readFileSync(filePath, 'utf8')
// 			const object = yaml.load(content)
// 			if (types[object.type] == null) {
// 				types[object.type] = []
// 			}
// 			types[object.type].push(Object.assign(object, {__filePath: filePath}))
// 		})
// 		const markdownTemplates = []
// 		for (type in types) {
// 			const entryLength = types[type].length
// 			const typeHeader = [`${argv.config.typeHeading} ${capitalizeFirst(type)} (${entryLength} change${entryLength > 1 ? 's' : ''})`]
// 			const entries = types[type].map(entry => {
// 				const rendered = utilty.renderTemplate(argv, entry)
// 				return `- ${rendered}`
// 			})
// 			markdownTemplates.push(typeHeader + '\n\n' + entries.join('\n'))
// 		}
// 		const newContent = `${argv.config.releaseHeading} ` + newVersion + '\n\n\n' + markdownTemplates.join('\n\n\n') + '\n'
// 		const changelogFileName = argv.out || argv.config.changelogFile
// 		const releaseFilePath = path.join(pathToWrite, changelogFileName)
// 		let currentContent = ''
// 		try {
// 			currentContent = fs.readFileSync(releaseFilePath, 'utf8')
// 		} catch (err) {}
// 		const updated = updateChangelogFile(currentContent, newVersion, newContent, argv.config.releaseHeading)
//
// 		if (argv.dryRun) {
// 			console.log(newContent)
// 			return
// 		}
// 		fs.writeFileSync(releaseFilePath, updated)
// 		console.log('changelog was updated', releaseFilePath)
// 		files.forEach(file => {
// 			fs.unlinkSync(file)
// 		})
// 		console.log(`removed ${files.length} entries from ${pathToScan}`)
// 	}
