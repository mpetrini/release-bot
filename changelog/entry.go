package changelog

import (
	"io/ioutil"
	"log"

	"github.com/go-yaml/yaml"
)

type Entry struct {
	Title        string `yaml:"title"`
	Type         string `yaml:"type"`
	MergeRequest string `yaml:"merge_request"`
	Client       string `yaml:"client"`
}

func LoadEntry(Path string) Entry {
	content, err := ioutil.ReadFile(Path)
	var m Entry

	err = yaml.Unmarshal([]byte(content), &m)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return m
}
