package changelog

type TemplateBlocks struct {
	MergeRequest string `yaml:"merge_request"`
	Author       string `yaml:"author"`
	Client       string `yaml:"client"`
}
