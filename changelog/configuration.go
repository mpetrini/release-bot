package changelog

import (
	"io/ioutil"
	"log"

	"github.com/go-yaml/yaml"
)

type Configuration struct {
	ReleasedDir      string         `yaml:"releasedDir"`
	UnreleasedDir    string         `yaml:"unreleasedDir"`
	ReleaseHeading   string         `yaml:"releaseHeading"`
	TypeHeading      string         `yaml:"typeHeading"`
	ChangelogFile    string         `yaml:"changelogFile"`
	EntryTemplate    string         `yaml:"entryTemplate"`
	StripLinkPattern string         `yaml:"stripLinkPattern"`
	TemplateBlocks   TemplateBlocks `yaml:"templateBlocks"`
}

func LoadConfiguration(path string) Configuration {
	content, err := ioutil.ReadFile(path)
	var m Configuration

	err = yaml.Unmarshal([]byte(content), &m)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return m
}
