package main

import (
	"io/ioutil"
	"log"
	"net/url"
	"os"

	"gitlab.com/mpetrini/release-bot/changelog"
	git "gopkg.in/src-d/go-git.v4"
)

type Release struct {
	GitURL         string
	ReleaseVersion string
}

func (p Release) GetURLWithAuthentication() string {
	UrlWithAuthentication := ""
	URLParsed, err := url.Parse(p.GitURL)

	if err != nil {
		log.Fatal(err)
	}

	UrlWithAuthentication = URLParsed.Scheme + "://" + os.Getenv("GITLAB_USER") + ":" + os.Getenv("PRIVATE_TOKEN") + "@" + URLParsed.Host
	if len(URLParsed.Port()) > 0 {
		UrlWithAuthentication += ":" + URLParsed.Port()
	}
	UrlWithAuthentication += URLParsed.RequestURI()

	return UrlWithAuthentication
}

func (p Release) CreateDirectory() string {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}

	return dir
}

func (p Release) Clone() (*git.Repository, string) {
	dir := p.CreateDirectory()
	repository, err := git.PlainClone(dir, false, &git.CloneOptions{
		URL:      p.GetURLWithAuthentication(),
		Progress: os.Stdout,
	})

	if err != nil {
		log.Fatal("An error occured on git clone : " + err.Error())
	}

	return repository, dir
}

func (p Release) ExecuteChangelog(Directory string, Configuration changelog.Configuration) {
	Rel := changelog.Release{
		ReleaseVersion: p.ReleaseVersion,
		Configuration:  Configuration,
		PathToScan:     Directory + "/" + Configuration.UnreleasedDir,
		PathToWrite:    Directory + "/" + Configuration.ReleasedDir,
	}

	Rel.Create()
}

func (p Release) CreateRelease() {
	//_, Directory := p.Clone()
	Directory := "/Applications/MAMP/htdocs/qrieu"
	p.ExecuteChangelog(Directory, changelog.LoadConfiguration(Directory+"/.changelog-manager.yml"))
}
