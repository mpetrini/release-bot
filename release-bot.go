package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"github.com/joho/godotenv"
	"github.com/manifoldco/promptui"
	"gitlab.com/mpetrini/release-bot/types"
)

var TypeItems = []string{"project", "group"}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func GetType() string {
	prompt := promptui.Select{
		Label: "Choose a type of report : ",
		Items: TypeItems,
	}

	_, result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return ""
	}
	return result
}

func GetListType(Endpoint types.Base, Type string) types.Reportable {
	prompt := promptui.Select{}
	template := &promptui.SelectTemplates{
		Active:   "{{ .Title | underline }}",
		Inactive: "{{ .Title }}",
		Selected: "{{ .Title | underline }}",
	}
	label := "Choose a " + Type + " : "

	if Type == "project" {
		items := types.Project{}.List(Endpoint)

		prompt = promptui.Select{
			Label:     label,
			Items:     items,
			Templates: template,
		}

		i, _, _ := prompt.Run()

		return items[i]
	} else if Type == "group" {
		items := types.Group{}.List(Endpoint)

		prompt = promptui.Select{
			Label:     label,
			Items:     items,
			Templates: template,
		}

		i, _, _ := prompt.Run()

		return items[i]
	}

	return types.Group{}
}

func CreateReport(TypeOfReport string, TypeChoosen types.Reportable) {
	Report{Item: TypeChoosen}.Create()
}

func ReportsOptions(flagTypeOfReport string, flagTypeID int) {
	var ItemChoosen types.Reportable
	typeOfReport := flagTypeOfReport
	typeChoosen := flagTypeID
	base := types.Base{}

	if len(typeOfReport) == 0 {
		typeOfReport = GetType()
	} else if !Contains(TypeItems, typeOfReport) {
		fmt.Println("Type '" + typeOfReport + "' not recognized")
	}

	if Contains(TypeItems, typeOfReport) && typeChoosen == 0 {
		ItemChoosen = GetListType(base, typeOfReport)
	} else if typeChoosen != 0 {
		if typeOfReport == "group" {
			ItemChoosen = types.Group{}.Get(base, typeChoosen)
		} else if typeOfReport == "project" {
			ItemChoosen = types.Project{}.Get(base, typeChoosen)
		}
	}

	CreateReport(typeOfReport, ItemChoosen)
}

func ReleasesOptions(GitURL string, VersionNumber string) {
	var prompt promptui.Prompt
	if len(GitURL) == 0 {
		prompt = promptui.Prompt{Label: "Url of the repository"}
		GitURL, _ = prompt.Run()
	}
	if len(VersionNumber) == 0 {
		prompt = promptui.Prompt{Label: "Release Version"}
		VersionNumber, _ = prompt.Run()
	}

	if len(GitURL) == 0 {
		log.Fatal("Please enter an URL Repository")
	}
	if len(VersionNumber) == 0 {
		log.Fatal("Please enter a Release Version")
	}

	p := Release{
		GitURL:         GitURL,
		ReleaseVersion: VersionNumber,
	}

	p.CreateRelease()
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	isReport := flag.Bool("report", false, "Print a report of a milestone")
	flagTypeOfReport := flag.String("type", "", strings.Join(TypeItems, "|"))
	flagTypeID := flag.Int("type-id", 0, "ID of the group or the project")

	isRelease := flag.Bool("release", false, "Create a new version of the app")
	flagGitURL := flag.String("git-url", "", "URL of the repository")
	flagVersionNumber := flag.String("version", "", "Release Version")

	flag.Parse()

	if *isReport {
		ReportsOptions(*flagTypeOfReport, *flagTypeID)
	} else if *isRelease {
		ReleasesOptions(*flagGitURL, *flagVersionNumber)
	} else {
		fmt.Println("Please choose a type between '-report' or '-release'")
	}
}
