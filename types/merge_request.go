package types

type MergeRequest struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	WebUrl string `json:"web_url"`
}
