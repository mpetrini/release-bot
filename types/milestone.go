package types

type Milestone struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
}
