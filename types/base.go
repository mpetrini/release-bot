package types

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
)

const GITLAB_ACCESS_GUEST = "10"
const GITLAB_ACCESS_REPORTER = "20"
const GITLAB_ACCESS_DEVELOPER = "30"
const GITLAB_ACCESS_MAINTAINER = "40"
const GITLAB_ACCESS_OWNER = "50"

type Base struct {
}

func (p Base) GetURLWithParameters(Endpoint string, Parametres map[string]string) string {
	Parametres["private_token"] = os.Getenv("PRIVATE_TOKEN")
	Parameters := ""
	for key, value := range Parametres {
		if len(Parameters) > 0 {
			Parameters += "&"
		}
		Parameters += key + "=" + value
	}

	return os.Getenv("GITLAB_ENDPOINT") + Endpoint + "?" + url.PathEscape(Parameters)
}

func (p Base) Launch(Endpoint string, Parameters map[string]string) (data []byte) {
	response, err := http.Get(p.GetURLWithParameters(Endpoint, Parameters))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		return data
	}
	return
}
