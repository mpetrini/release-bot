package types

import (
	"encoding/json"
	"strconv"
	"time"
)

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

type Issue struct {
	ID        int       `json:"id"`
	IID       int       `json:"iid"`
	ProjectID int       `json:"project_id"`
	Author    User      `json:"author"`
	Assignees []User    `json:"assignees"`
	Title     string    `json:"title"`
	Labels    []string  `json:"labels"`
	ClosedAt  time.Time `json:"closed_at"`
	WebURL    string    `json:"web_url"`
	Weight    int       `json:"weight"`
	TimeStats TimeStat  `json:"time_stats"`
}

func (p Issue) GetMergeRequestsRelated(Endpoint Base, Item Reportable) []MergeRequest {
	keys := make([]MergeRequest, 0)

	json.Unmarshal(
		Endpoint.Launch("projects/"+strconv.Itoa(p.ProjectID)+"/issues/"+strconv.Itoa(p.IID)+"/related_merge_requests", make(map[string]string)),
		&keys,
	)

	return keys
}

func (p Issue) HasStarted(Endpoint Base, Item Reportable) bool {
	return len(p.GetMergeRequestsRelated(Endpoint, Item)) > 0
}

func (p Issue) HasLabel(Label string) bool {
	return Contains(p.Labels, Label)
}

func (p Issue) IsBlocked() bool {
	return p.HasLabel("Blocked")
}

func (p Issue) IsDone() bool {
	return p.HasLabel("Done")
}

func (p Issue) IsClosed() bool {
	return p.ClosedAt.Year() != 1
}
