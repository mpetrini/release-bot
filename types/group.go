package types

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type Group struct {
	ID       int    `json:"id"`
	FullPath string `json:"full_path"`
}

func (p Group) GetNameOfReportable() string {
	return "groups"
}

func (p Group) Get(Endpoint Base, ID int) Reportable {
	var keys Group
	json.Unmarshal(
		Endpoint.Launch("/groups/"+strconv.Itoa(ID)+"", make(map[string]string)),
		&keys,
	)

	return keys
}

func (p Group) List(Endpoint Base) []Group {
	keys := make([]Group, 0)
	Parameters := make(map[string]string)
	Parameters["min_access_level"] = GITLAB_ACCESS_DEVELOPER

	json.Unmarshal(
		Endpoint.Launch("groups", Parameters),
		&keys,
	)

	return keys
}

func (p Group) GetMilestones(Endpoint Base, Parameters map[string]string) []Milestone {
	keys := make([]Milestone, 0)
	fmt.Println("/groups/" + strconv.Itoa(p.ID) + "/milestones")
	json.Unmarshal(
		Endpoint.Launch("/groups/"+strconv.Itoa(p.ID)+"/milestones", Parameters),
		&keys,
	)

	return keys
}

func (p Group) GetIssues(Endpoint Base, Milestone Milestone, Parameters map[string]string) []Issue {
	keys := make([]Issue, 0)
	json.Unmarshal(
		Endpoint.Launch("/groups/"+strconv.Itoa(p.ID)+"/milestones/"+strconv.Itoa(Milestone.ID)+"/issues", Parameters),
		&keys,
	)

	return keys
}

func (p Group) GetID() int {
	return p.ID
}

func (p Group) Title() string {
	return strconv.Itoa(p.ID) + " - " + p.FullPath
}
