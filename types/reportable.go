package types

type Reportable interface {
	GetNameOfReportable() string
	GetID() int
	Get(Endpoint Base, ID int) Reportable
	GetMilestones(Endpoint Base, Parameters map[string]string) []Milestone
	GetIssues(Endpoint Base, Milestone Milestone, Parameters map[string]string) []Issue
	Title() string
}
