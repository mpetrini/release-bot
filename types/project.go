package types

import (
	"encoding/json"
	"strconv"
)

type Project struct {
	ID                int    `json:"id"`
	NameWithNamespace string `json:"name_with_namespace"`
}

func (p Project) GetNameOfReportable() string {
	return "project"
}

func (p Project) Get(Endpoint Base, ID int) Reportable {
	var keys Project
	json.Unmarshal(
		Endpoint.Launch("/"+p.GetNameOfReportable()+"/"+strconv.Itoa(ID)+"", make(map[string]string)),
		&keys,
	)

	return keys
}

func (p Project) List(Endpoint Base) []Project {
	keys := make([]Project, 0)
	Parameters := make(map[string]string)
	Parameters["order_by"] = "path"
	Parameters["min_access_level"] = GITLAB_ACCESS_DEVELOPER
	Parameters["sort"] = "desc"

	json.Unmarshal(
		Endpoint.Launch(p.GetNameOfReportable(), Parameters),
		&keys,
	)

	return keys
}

func (p Project) GetMilestones(Endpoint Base, Parameters map[string]string) []Milestone {
	keys := make([]Milestone, 0)
	json.Unmarshal(
		Endpoint.Launch("/"+p.GetNameOfReportable()+"/"+strconv.Itoa(p.ID)+"/milestones", Parameters),
		&keys,
	)

	return keys
}

func (p Project) GetIssues(Endpoint Base, Milestone Milestone, Parameters map[string]string) []Issue {
	keys := make([]Issue, 0)
	json.Unmarshal(
		Endpoint.Launch("/projects/"+strconv.Itoa(p.ID)+"/issues", Parameters),
		&keys,
	)

	return keys
}

func (p Project) GetID() int {
	return p.ID
}

func (p Project) Title() string {
	return strconv.Itoa(p.ID) + " - " + p.NameWithNamespace
}
