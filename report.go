package main

import (
	"fmt"
	"strconv"

	"github.com/manifoldco/promptui"
	"gitlab.com/mpetrini/release-bot/types"
)

type Report struct {
	Item     types.Reportable
	Endpoint types.Base
	Issues   []types.Issue
}

func (p Report) GetMilestones() []types.Milestone {
	Parameters := make(map[string]string)
	Parameters["state"] = "active"
	return p.Item.GetMilestones(p.Endpoint, Parameters)
}

func (p Report) ChooseMilestone() types.Milestone {
	Milestones := p.GetMilestones()

	prompt := promptui.Select{
		Label: "Choose a milestone : ",
		Items: Milestones,
	}

	i, _, _ := prompt.Run()

	return Milestones[i]
}

func (p Report) GetWeight(Issues []types.Issue) int {
	report := 0
	for i := 0; i < len(Issues); i++ {
		report += Issues[i].Weight
	}
	return report
}

func (p Report) GetTimeEstimate(Issues []types.Issue) int {
	report := 0
	for i := 0; i < len(Issues); i++ {
		report += Issues[i].TimeStats.TimeEstimate
	}
	return report
}

func (p Report) GetTimeSpent(Issues []types.Issue) int {
	report := 0
	for i := 0; i < len(Issues); i++ {
		report += Issues[i].TimeStats.TotalTimeSpent
	}
	return report
}

func (p Report) TemplateTypeOfIssue(Issues []types.Issue) string {
	report := ""
	for i := 0; i < len(Issues); i++ {
		report += p.TemplateIssue(Issues[i])
	}
	return report
}

func (p Report) TemplateIssue(Issue types.Issue) string {
	report := Issue.Title + " (" + Issue.WebURL + ") ("
	for j := 0; j < len(Issue.Assignees); j++ {
		if j > 0 {
			report += " / "
		}
		report += "@" + Issue.Assignees[j].Username
	}
	report += ")\n"

	for j := 0; j < len(Issue.Labels); j++ {
		if j > 0 {
			report += " / "
		}
		report += Issue.Labels[j]
	}
	report += "\n"

	return report
}

func (p Report) GetIssuesClosedOrDone() []types.Issue {
	var issues []types.Issue
	for i := 0; i < len(p.Issues); i++ {
		if p.Issues[i].IsClosed() || p.Issues[i].IsDone() {
			issues = append(issues, p.Issues[i])
		}
	}
	return issues
}

func (p Report) GetIssuesBlocked() []types.Issue {
	var issues []types.Issue
	for i := 0; i < len(p.Issues); i++ {
		if p.Issues[i].IsBlocked() {
			issues = append(issues, p.Issues[i])
		}
	}
	return issues
}

func (p Report) GetIssuesStarted() []types.Issue {
	var issues []types.Issue
	for i := 0; i < len(p.Issues); i++ {
		if !p.Issues[i].IsClosed() && !p.Issues[i].IsDone() && p.Issues[i].HasStarted(p.Endpoint, p.Item) {
			issues = append(issues, p.Issues[i])
		}
	}
	return issues
}

func (p Report) GetIssuesOfTheSprint() []types.Issue {
	var issues []types.Issue
	for i := 0; i < len(p.Issues); i++ {
		if !p.Issues[i].HasLabel("TMA") {
			issues = append(issues, p.Issues[i])
		}
	}
	return issues
}

func (p Report) Create() {
	report := ""
	Parameters := make(map[string]string)
	Parameters["per_page"] = "100"
	p.Issues = p.Item.GetIssues(p.Endpoint, p.ChooseMilestone(), Parameters)

	IssuesOfTheSprint := p.GetIssuesOfTheSprint()
	IssuesClosed := p.GetIssuesClosedOrDone()
	IssuesBlocked := p.GetIssuesBlocked()
	IssuesStarted := p.GetIssuesStarted()

	report = "Weight effectué/prévu : " + strconv.Itoa(p.GetWeight(IssuesClosed)) + "/" + strconv.Itoa(p.GetWeight(p.Issues)) + "\n"
	report += "Temps passé/prévu : " + strconv.Itoa(p.GetTimeSpent(IssuesOfTheSprint)/60) + "min / " + strconv.Itoa(p.GetTimeEstimate(IssuesOfTheSprint)/60) + "min\n"
	report += "*Nombre d'issues terminés : " + strconv.Itoa(len(IssuesClosed)) + "*\n"
	report += p.TemplateTypeOfIssue(IssuesClosed) + "\n"
	report += "*Nombre d'issues bloquées : " + strconv.Itoa(len(IssuesBlocked)) + "*\n"
	report += p.TemplateTypeOfIssue(IssuesBlocked) + "\n"
	report += "*Nombre d'issues commencées : " + strconv.Itoa(len(IssuesStarted)) + "*\n"
	report += p.TemplateTypeOfIssue(IssuesStarted) + "\n"

	fmt.Println(report)
}
