# Release Bot

Release Bot plugged with Gitlab to export each sprint some stats

API Gitlab : https://docs.gitlab.com/ee/api/

release-bot -report
release-bot -report -type project  
release-bot -report -type group  
release-bot -report -type-id 2  
release-bot -report -type group -type-id 2  
release-bot -report -type project -type-id 2

release-bot -release  
release-bot -release -version=1.1.0  
release-bot -release -git-url=https://gitlab.com/mpetrini/release-bot  
release-bot -release -git-url=https://gitlab.com/mpetrini/release-bot -version=1.1.0  
